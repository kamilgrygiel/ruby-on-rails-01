class User < ApplicationRecord
  validates_presence_of :job
  validates_numericality_of :age

  has_one :car
  geocoded_by :address
  after_validation :geocode, if: :address_changed?


  self.per_page = 10

  def full_name
    "#{name} #{surname}"
  end
end
