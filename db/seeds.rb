200.times do |i|
  p "Creating user no #{i}"
  user = User.create(name: Faker::StarWars.character,
              surname: Faker::StarWars.specie,
              age: rand(100),
              city: Faker::StarWars.planet,
              job: Faker::StarWars.quote)
  p "Created user #{user.full_name}"
end

p "Finished"